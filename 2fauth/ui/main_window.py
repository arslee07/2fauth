# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'resources/ui/main_window.ui'
#
# Created by: PyQt5 UI code generator 5.15.7
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(377, 370)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/src/resources/icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.items = QtWidgets.QListWidget(self.centralwidget)
        self.items.setProperty("showDropIndicator", False)
        self.items.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.items.setObjectName("items")
        self.verticalLayout.addWidget(self.items)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.expires = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.expires.sizePolicy().hasHeightForWidth())
        self.expires.setSizePolicy(sizePolicy)
        self.expires.setObjectName("expires")
        self.horizontalLayout.addWidget(self.expires)
        self.exportBtn = QtWidgets.QPushButton(self.centralwidget)
        self.exportBtn.setObjectName("exportBtn")
        self.horizontalLayout.addWidget(self.exportBtn)
        self.importBtn = QtWidgets.QPushButton(self.centralwidget)
        self.importBtn.setObjectName("importBtn")
        self.horizontalLayout.addWidget(self.importBtn)
        self.add = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add.sizePolicy().hasHeightForWidth())
        self.add.setSizePolicy(sizePolicy)
        self.add.setObjectName("add")
        self.horizontalLayout.addWidget(self.add)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "2FAuth"))
        self.expires.setText(_translate("MainWindow", "Expires in: 3 sec"))
        self.exportBtn.setText(_translate("MainWindow", "Export"))
        self.importBtn.setText(_translate("MainWindow", "Import"))
        self.add.setText(_translate("MainWindow", "Add"))
import src_rc
