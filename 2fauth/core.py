import csv
import sqlite3
import time
from dataclasses import dataclass
from typing import Any

from pyotp import TOTP, parse_uri, random_base32

QUERY_CREATE_TABLE = """
CREATE TABLE IF NOT EXISTS mfas (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name STRING NOT NULL,
    secret STRING NOT NULL
)
"""
QUERY_FETCH_ALL = "SELECT * FROM mfas"
QUERY_DELETE = "DELETE FROM mfas WHERE id = ?"
QUERY_DELETE_ALL = "DELETE FROM mfas"
QUERY_ADD = "INSERT INTO mfas (name, secret) VALUES (?, ?)"
QUERY_UPDATE = "UPDATE mfas SET name = ? WHERE id = ?"


@dataclass
class Mfa:
    id: int
    name: str
    totp: TOTP


class MfaManager:
    __con: sqlite3.Connection

    def __init__(self):
        self.__con = sqlite3.connect("db.db")
        self.__exec(QUERY_CREATE_TABLE)

        # self.__insert_test_data()

    def __insert_test_data(self):
        for name in ["Google", "Discord", "GitHub"]:
            secret = random_base32()
            self.add(name, secret)

    def __exec(self, query: str, args=()) -> None:
        cur = self.__con.cursor()
        cur.execute(query, args)
        self.__con.commit()

    def __fetchall(self, query: str, args=()) -> list[Any]:
        cur = self.__con.cursor()
        return cur.execute(query, args).fetchall()

    @property
    def time_left(self) -> float:
        return 30 - (time.time() % 30)

    def import_from_csv(self, file_path: str) -> None:
        self.__exec(QUERY_DELETE_ALL)
        with open(file_path, "r", newline="") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                self.add(row[0], row[1])

    def export_to_csv(self, file_path: str) -> None:
        with open(file_path, "w", newline="") as csvfile:
            writer = csv.writer(csvfile)
            for item in self.get_all():
                writer.writerow((item.name, item.totp.secret))

    def get_all(self) -> list[Mfa]:
        res = self.__fetchall("SELECT id, name, secret FROM mfas")
        return [Mfa(i[0], i[1], TOTP(i[2])) for i in res]

    def delete(self, id: int) -> None:
        self.__exec(QUERY_DELETE, (id,))

    def add(self, name: str, secret: str) -> None:
        self.__exec(QUERY_ADD, (name, secret))

    def add_by_uri(self, name: str, uri: str) -> None:
        self.__exec(QUERY_ADD, (name, parse_uri(uri).secret))

    def update(self, id: int, new_name: str) -> None:
        self.__exec(QUERY_UPDATE, (new_name, id))
