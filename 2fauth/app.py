import sys

from pyotp import parse_uri
from PyQt5.QtCore import QTimer, pyqtSignal
from PyQt5.QtWidgets import (QApplication, QDialog, QFileDialog,
                             QListWidgetItem, QMainWindow, QMessageBox,
                             QWidget)

from core import Mfa, MfaManager
from ui import Ui_AddDialog, Ui_EditDialog, Ui_MainWindow, Ui_MfaItem


class AddDialog(QDialog, Ui_AddDialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

    def accept(self):
        if self.name.text() == "":
            QMessageBox.critical(self, "Error", "Name cannot be empty")
        else:
            try:
                totp = parse_uri(self.totp.text())
            except:
                QMessageBox.critical(self, "Error", "Invalid TOTP link")
            else:
                self.data = (self.name.text(), totp.secret)
                return super().accept()


class EditDialog(QDialog, Ui_EditDialog):
    def __init__(self, model: Mfa):
        super().__init__()
        self.model = model

        self.setupUi(self)

        self.totp.setText(self.model.totp.provisioning_uri())
        self.name.setText(self.model.name)

    def accept(self):
        if self.name.text() == "":
            QMessageBox.critical(self, "Error", "Name cannot be empty")
        else:
            return super().accept()


class MfaWidget(QWidget, Ui_MfaItem):
    updateRequested = pyqtSignal()

    def __init__(self, model: Mfa, manager: MfaManager):
        super().__init__()

        self.model = model
        self.manager = manager

        self.setupUi(self)

        self.clipboard = QApplication.clipboard()
        self.copyBtn.clicked.connect(self.copy_code)
        self.deleteBtn.clicked.connect(self.delete_item)
        self.editBtn.clicked.connect(self.edit_item)

        self.update_timer = QTimer()
        self.update_timer.setInterval(250)
        self.update_timer.timeout.connect(self.update_code)
        self.update_timer.start()

        self.update_code()

    def delete_item(self):
        if (
            QMessageBox.question(
                self,
                "Confirmation",
                "Are you sure you want to delete the key? This action is irreversible.",
                QMessageBox.Yes | QMessageBox.No,
            )
            == QMessageBox.Yes
        ):
            self.manager.delete(self.model.id)
            self.updateRequested.emit()

    def update_code(self):
        self.title.setText(self.model.name)
        self.code.setText(str(self.model.totp.now()))

    def copy_code(self):
        self.clipboard.clear(self.clipboard.Clipboard)
        self.clipboard.setText(
            str(self.model.totp.now()), mode=self.clipboard.Clipboard
        )

    def edit_item(self):
        if (dialog := EditDialog(self.model)).exec():
            self.manager.update(self.model.id, dialog.name.text())
            self.updateRequested.emit()


class App(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.manager = MfaManager()

        self.setupUi(self)

        self.update()

        self.expire_timer = QTimer()
        self.expire_timer.setInterval(100)
        self.expire_timer.timeout.connect(self.update_timer)
        self.expire_timer.start()

        self.add.clicked.connect(self.add_item)

        self.exportBtn.clicked.connect(self.export_csv)
        self.importBtn.clicked.connect(self.import_csv)

    def add_item(self):
        if (dialog := AddDialog()).exec():
            self.manager.add(dialog.data[0], dialog.data[1])
            self.update()

    def update(self):
        self.update_timer()
        self.build_mfas()

    def update_timer(self):
        time = round(self.manager.time_left)
        self.expires.setText(f"Expires in: {time} secs")

    def delete_mfa(self, model):
        self.manager.delete(model.id)
        self.build_mfas()

    def import_csv(self):
        file_path, ok = QFileDialog.getOpenFileName(
            self, "Import database", "", "CSV table (*.csv);;All Files (*)"
        )

        if ok:
            self.manager.import_from_csv(file_path)
            self.update()

    def export_csv(self):
        file_path, ok = QFileDialog.getSaveFileName(
            self, "Export database", "", "CSV table (*.csv);;All Files (*)"
        )

        if ok:
            self.manager.export_to_csv(file_path)

    def build_mfas(self):
        self.items.clear()
        data = self.manager.get_all()

        for item in data:
            widget = MfaWidget(item, self.manager)

            widget.updateRequested.connect(self.update)

            widget_list_item = QListWidgetItem(self.items)
            widget_list_item.setSelected(True)
            widget_list_item.setSizeHint(widget.sizeHint())

            self.items.addItem(widget_list_item)
            self.items.setItemWidget(widget_list_item, widget)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
    ex.show()
    sys.exit(app.exec_())
